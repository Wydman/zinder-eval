import {Injectable, Input} from '@angular/core';
import {HttpClient,} from '@angular/common/http';
import {Observable} from 'rxjs';
import {People} from '../models/people';
import {Profils} from '../models/profils';
import {Match} from '../models/match';
import {Interet} from './interet';


@Injectable()
export class PeopleService {

  constructor(private http: HttpClient) {
  }

  getPeopleList(): Observable<Profils> {
    return this.http.get<Profils>('http://localhost:8088/zinder/profils');
  }

  getMatchs(): Observable<Match[]> {
    return this.http.get<Match[]>('http://localhost:8088/zinder/matchs');
  }


  addMatch(id:string,profilName:string,matchStatus:boolean): Observable<Match> {
    console.log(id);
    return this.http.post<Match>(`http://localhost:8088/zinder/profils/${id}/match`, new Match(id,profilName,matchStatus));
  }

  deleteMatch(id:string): Observable<Match> {
    console.log(id);
    return this.http.delete<Match>(`http://localhost:8088/zinder/matchs/${id}`);
  }
  getInterets(): Observable<Interet[]> {
    return this.http.get<Interet[]>('http://localhost:8088/zinder/interets');
  }
}
