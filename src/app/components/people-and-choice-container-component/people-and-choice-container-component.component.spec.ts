import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleAndChoiceContainerComponentComponent } from './people-and-choice-container-component.component';

describe('PeopleAndChoiceContainerComponentComponent', () => {
  let component: PeopleAndChoiceContainerComponentComponent;
  let fixture: ComponentFixture<PeopleAndChoiceContainerComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleAndChoiceContainerComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleAndChoiceContainerComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
