import {Component, Input, OnInit} from '@angular/core';
import {People} from '../../models/people';
import {Match} from '../../models/match';
import {PeopleService} from '../../services/people-service';

@Component({
  selector: 'app-people-and-choice-container-component',
  templateUrl: './people-and-choice-container-component.component.html',
  styleUrls: ['./people-and-choice-container-component.component.css']
})
export class PeopleAndChoiceContainerComponentComponent implements OnInit {
  @Input() people: People;
  @Input() displayChoice: boolean = true;
  @Input() match: Match;

  constructor(private peopleService:PeopleService) {
  }

  ngOnInit() {
  }

  getPeopleFromMatch(match:Match){

  }

}
