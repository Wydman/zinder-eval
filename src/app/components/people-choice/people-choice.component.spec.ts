import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleChoiceComponent } from './people-choice.component';

describe('PeopleChoiceComponent', () => {
  let component: PeopleChoiceComponent;
  let fixture: ComponentFixture<PeopleChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
