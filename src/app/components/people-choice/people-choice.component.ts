import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {People} from '../../models/people';
import {PeopleService} from '../../services/people-service';
import {ChoicesService} from '../../services/choices-service';

@Component({
  selector: 'app-people-choice',
  templateUrl: './people-choice.component.html',
  styleUrls: ['./people-choice.component.css']
})
export class PeopleChoiceComponent implements OnInit {
  @Input() people: People;

  constructor(private peopleService: PeopleService, private choicesService: ChoicesService) {
  }

  ngOnInit() {
  }

  likeThatPeople() {
    if (!this.choicesService.peopleListWhoAreMatchted.includes(this.people)) {
      this.peopleService.addMatch(this.people.id, this.people.prenom + ' ' + this.people.nom, true).subscribe();
    }
    this.choicesService.nextPeople();
  }

//TODO pas le temps de refacto pour le moment
  dislikeThatPeople() {
    this.peopleService.addMatch(this.people.id, this.people.prenom + ' ' + this.people.nom, false).subscribe();
    this.choicesService.nextPeople();
  }
}
