import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LobbyComponentComponent } from './components/lobby-component/lobby-component.component';
import { PeopleDisplayComponent } from './components/people-display/people-display.component';
import { PeopleChoiceComponent } from './components/people-choice/people-choice.component';
import { PeopleListComponentComponent } from './components/people-list-component/people-list-component.component';
import { PeopleAndChoiceContainerComponentComponent } from './components/people-and-choice-container-component/people-and-choice-container-component.component';
import {PeopleService} from './services/people-service';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {ChoicesService} from './services/choices-service';
import { MatchsPreviewComponent } from './components/matchs-preview/matchs-preview.component';

const  appRoutes:Routes =[
  { path: 'stats', component:MatchsPreviewComponent },
 // { path: 'user', component:UserSidePanelComponent },
 // { path: 'user/generate', component:UserGenerateRandomQuestionPanelComponent },
 // { path: 'user/answer', component:UserAnswerQuizPanelComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LobbyComponentComponent,
    PeopleDisplayComponent,
    PeopleChoiceComponent,
    PeopleListComponentComponent,
    PeopleAndChoiceContainerComponentComponent,
    MatchsPreviewComponent
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    FormsModule
  ],
  providers: [PeopleService,ChoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
