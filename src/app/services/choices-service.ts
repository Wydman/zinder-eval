import {EventEmitter, Injectable, Input, Output} from '@angular/core';
import {HttpClient,} from '@angular/common/http';
import {Observable} from 'rxjs';
import {People} from '../models/people';
import {Profils} from '../models/profils';
import {PeopleService} from './people-service';


@Injectable()
export class ChoicesService {

  @Output() onNextPeople: EventEmitter<People> = new EventEmitter();

  peopleListWhoAreMatchted:People[]= [];

  constructor(private peopleService:PeopleService) {
  }

  nextPeople(){
    this.onNextPeople.emit();
  }

}
