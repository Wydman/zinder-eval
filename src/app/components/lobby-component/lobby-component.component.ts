import {Component, Input, OnInit} from '@angular/core';
import {PeopleService} from '../../services/people-service';
import {ChoicesService} from '../../services/choices-service';

@Component({
  selector: 'app-lobby-component',
  templateUrl: './lobby-component.component.html',
  styleUrls: ['./lobby-component.component.css']
})
export class LobbyComponentComponent implements OnInit {

  @Input() showAppListComponent: boolean = true;
  @Input() showMatchPreviewComponent: boolean = false;

  constructor(private peopleService:PeopleService, private choicesService:ChoicesService) {
  }

  ngOnInit() {
  }

  cleanMatchs(){
    this.choicesService.peopleListWhoAreMatchted.forEach(people=>{
      this.peopleService.deleteMatch(people.matchID).subscribe(()=>{
        this.choicesService.peopleListWhoAreMatchted = [];
      });
    })
  }


}
