import {People} from './people';

export class Profils {
  profils: People[] = [];
  nbProfils: number;

  constructor(profils: People[], nbProfils: number) {
    this.profils = profils;
    this.nbProfils = nbProfils;
  }
}
