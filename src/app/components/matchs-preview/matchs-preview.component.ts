import { Component, OnInit } from '@angular/core';
import {ChoicesService} from '../../services/choices-service';

@Component({
  selector: 'app-matchs-preview',
  templateUrl: './matchs-preview.component.html',
  styleUrls: ['./matchs-preview.component.css']
})
export class MatchsPreviewComponent implements OnInit {

  constructor(private choicesService:ChoicesService) { }

  ngOnInit() {
    console.log(this.choicesService.peopleListWhoAreMatchted);
  }

  test(){
    console.log(this.choicesService.peopleListWhoAreMatchted);
  }
}
