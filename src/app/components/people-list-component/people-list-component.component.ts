import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {People} from '../../models/people';
import {PeopleService} from '../../services/people-service';
import {Match} from '../../models/match';
import {ChoicesService} from '../../services/choices-service';
import {Interet} from '../../services/interet';

@Component({
  selector: 'app-people-list-component',
  templateUrl: './people-list-component.component.html',
  styleUrls: ['./people-list-component.component.css']
})
export class PeopleListComponentComponent implements OnInit {

  peopleList: People[] = [];
  interetList: String[] = [];
  filter:string='';
  filterActivated:boolean= false;

  @Output() onSwitch: EventEmitter<any> = new EventEmitter();

  index: number = 0;


  @Input() nextPeopleToShow: People;
  @Input() showNextPeople: boolean = false;
  @Input() showList: boolean = false;

  constructor(private peopleService: PeopleService, private choicesService: ChoicesService) {
    this.getAllPeoples();
  }

  getAllPeoples() {
    this.peopleList = [];
    // console.log(this.peopleService.getPeopleList().subscribe());
    this.peopleService.getPeopleList().subscribe(profils => {
      if(!this.filterActivated){
        this.peopleList = profils.profils;
      }
      else{
        profils.profils.filter(people =>{
          return people.interets.includes(this.filter);
        });

      }
        this.nextPeopleToShow = this.peopleList[this.index];
        this.showNextPeople = true;
        this.getAllMatchs();
      }, error => {
        alert('Error');
      }
    );
  }

  getAllMatchs() {
    let matchList: Match[] = [];
    this.choicesService.peopleListWhoAreMatchted = [];
    this.peopleService.getMatchs().subscribe(matchs => {
      matchList = matchs;
      this.peopleList.forEach(people => {
        matchs.forEach(match => {
          if (match.profil === people.id && match.match) {
            people.matchID = match.id;
            this.choicesService.peopleListWhoAreMatchted.push(people);
          }
        });
      });
    });


    /*
    this.peopleService.getMatchs().subscribe(matchs => {
      this.peopleList.map(people => {
        matchs.forEach(match => {
          if (match.profil === people.id) {
            people.matchID = match.id;
            if (!this.bufferList.includes(people.id)) {
              this.choicesService.peopleListWhoAreMatchted.push(people);
              this.bufferList.push(people.id);
            }
          }
        });
      });
      console.log(this.choicesService.peopleListWhoAreMatchted);
      console.log(this.bufferList);
    });

     */
  }

  showAllPeoples() {
    this.showList = true;
    this.showNextPeople = false;
  }

  filterList(interet:string){
    this.filterActivated = true;
    console.log("this.qsdfqsdfqsdfsdf");
    this.filter = interet;
    this.getAllPeoples();

  }

  ngOnInit() {
    this.choicesService.onNextPeople.subscribe(() => {
      if (this.index < this.peopleList.length) {
        this.index++;
      } else {
        this.index = 0;
      }
      this.nextPeopleToShow = this.peopleList[this.index];
    });


    this.peopleService.getInterets().subscribe(interests=>{
     interests.forEach(interet=>{
       this.interetList.push(interet.nom);
     })
    });
  }


}
