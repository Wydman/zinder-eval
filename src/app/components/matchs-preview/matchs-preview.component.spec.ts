import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchsPreviewComponent } from './matchs-preview.component';

describe('MatchsPreviewComponent', () => {
  let component: MatchsPreviewComponent;
  let fixture: ComponentFixture<MatchsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
