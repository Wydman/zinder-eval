import {Component, Input, OnInit} from '@angular/core';
import {People} from '../../models/people';
import {PeopleService} from '../../services/people-service';

@Component({
  selector: 'app-people-display',
  templateUrl: './people-display.component.html',
  styleUrls: ['./people-display.component.css']
})
export class PeopleDisplayComponent implements OnInit {
  @Input() people: People;
  @Input() interetList:String[] = [];

  constructor(private  peopleService: PeopleService) {
  }

  ngOnInit() {
    this.peopleService.getInterets().subscribe(interets => {

      interets.forEach(interet =>{

        if(this.people.interets.includes(interet.id)){
          this.interetList.push(interet.nom);
        }
      });
    });
  }
}
